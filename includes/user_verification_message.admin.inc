<?php
/**
 * @file
 * Administrative page callbacks for teh User Verification Message module.
 */

/**
 * Page callback for the administration page.
 */
function user_verification_message_admin_page($form, &$form_state) {
  $form['user_verification_message_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Verification message'),
    '#description' => t('The verification message that will be displayed to users after registering for the site.'),
    '#required' => TRUE,
    '#default_value' => variable_get('user_verification_message_message', t('A welcome message with further instructions has been sent to your e-mail address.')),
  );
  $form['user_verification_message_resend'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display resend link.'),
    '#description' => t('If checked, a link to resend the verification email will be appended to the verification message.'),
    '#default_value' => variable_get('user_verification_message_resend', FALSE),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
    '#submit' => array('user_verification_message_admin_page_submit'),
  );

  return $form;
}

/**
 * Submit callback for the administration page.
 */
function user_verification_message_admin_page_submit($form, &$form_state) {
  variable_set('user_verification_message_message', $form_state['values']['user_verification_message_message']);
  variable_set('user_verification_message_resend', $form_state['values']['user_verification_message_resend']);

  drupal_set_message('User verification message settings saved.');
}
