<?php
/**
 * @file
 * Page callbacks for the User Verification Message module.
 */

/**
 * Page callback for the resend email page.
 */
function user_verification_message_resend_page($form, &$form_state, $user) {
  // Redisplay the verification message.
  $form_state['user'] = $user;
  user_verification_message_user_register_form_submit($form, $form_state);

  // Attempt to resend the verification email.
  $mail = _user_mail_notify(_user_verification_message_get_operation(), $user);
  if (!empty($mail)) {
    watchdog('user', 'Welcome message has been re-sent to %name at %email.', array('%name' => format_username($user), '%email' => $user->mail));
    drupal_set_message(t('Welcome message has been re-sent to %email', array('%email' => $user->mail)));
  }
  else {
    watchdog('user', 'There was an error re-sending welcome message to %name at %email', array('%name' => format_username($user), '%email' => $user->mail));
    drupal_set_message(t('There was an error re-sending welcome message to %email', array('%email' => $user->mail)), 'error');
  }

  // Redirect back to the previous page.
  drupal_goto(drupal_get_destination());
}
